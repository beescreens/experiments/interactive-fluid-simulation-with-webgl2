import {
  ConnectedSocket,
  MessageBody,
  OnGatewayConnection,
  OnGatewayDisconnect,
  SubscribeMessage,
  WebSocketGateway,
} from '@nestjs/websockets';
import { Socket } from 'socket.io';

type Point = {
  x: number;
  y: number;
};

@WebSocketGateway({
  cors: {
    origin: '*',
  },
})
export class AppGateway implements OnGatewayConnection, OnGatewayDisconnect {
  handleConnection(@ConnectedSocket() socket: Socket): void {
    console.log('A player has connected');
  }

  handleDisconnect(@ConnectedSocket() socket: Socket): void {
    console.log('A player has disconnected');
  }

  @SubscribeMessage('POINT_START')
  start(@ConnectedSocket() socket: Socket, @MessageBody() point: Point): void {
    console.log(`point start: ${JSON.stringify(point)}`);
    socket.broadcast.emit('POINT_START', point);
  }

  @SubscribeMessage('POINT_MOVE')
  move(@ConnectedSocket() socket: Socket, @MessageBody() point: Point): void {
    console.log(`point move: ${JSON.stringify(point)}`);
    socket.broadcast.emit('POINT_MOVE', point);
  }

  @SubscribeMessage('POINT_END')
  end(@ConnectedSocket() socket: Socket, @MessageBody() point: Point): void {
    console.log(`point end: ${JSON.stringify(point)}`);
    socket.broadcast.emit('POINT_END', point);
  }
}
