# Interactive fluid simulation with WebGL2

Interactive fluid simulation with WebGL2 and WebSockets highly inspired by the work of [Loïc Magne](https://github.com/loicmagne/webgl2_fluidsim/) on their implementation of the Stable Fluid algorithm.

## Installation

```sh
npm install
```

## Run the app

```sh
npm run start:dev
```

You can now access <http://localhost:3000> in two separate windows to interact with the simulation.
